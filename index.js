let boxElement = document.getElementById('mainBox');
let increamentButton = document.getElementById('increamentButton');
let decreamentButton = document.getElementById('decreamentButton');

increamentButton.addEventListener('click', function(event){
    let increamentHeightValue = boxElement.offsetHeight;
    let increamentWidhtValue = boxElement.offsetWidth;
    boxElement.style.height = increamentHeightValue + 50;
    boxElement.style.width = increamentWidhtValue + 50;

});
decreamentButton.addEventListener('click', function(event){
    let decreamentHeightValue = boxElement.offsetHeight;
    let decreamentWidhtValue = boxElement.offsetWidth;
    if (decreamentHeightValue > 250 && decreamentWidhtValue >250){
        boxElement.style.height = decreamentHeightValue - 50;
        boxElement.style.width = decreamentWidhtValue - 50;
    }
});